//
//  PriceMapAnnotationView.m
//  maprooms
//
//  Created by hy110831 on 8/2/16.
//  Copyright © 2016 hy110831. All rights reserved.
//

#import "PriceMapAnnotationView.h"
#import "PriceMapAnnotation.h"
#import <Masonry/Masonry.h>
#import <BlocksKit/BlocksKit.h>

@implementation PriceMapAnnotationView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(id)initWithAnnotation:(id<MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    if (self) {
        if ([self viewWithTag:8989] == nil) {
            self.priceLabel = [[UILabel alloc]init];
            self.priceLabel.tag = 8989;
            
            [self addSubview:self.priceLabel];
            
        }
        self.priceLabel.frame = CGRectMake(0, 0, 80, 30);
        self.image = [self imageFromColor:[UIColor clearColor] rect:self.priceLabel.frame];
        self.priceLabel.backgroundColor = [self unselectedBackgroundColor];
        self.priceLabel.textColor = [self unselectedTextColor];
        self.priceLabel.font = [UIFont systemFontOfSize:16];
        self.layer.cornerRadius = 5;
        self.layer.borderColor = [self borderColor].CGColor;
        self.layer.borderWidth = 1.5;
        self.layer.masksToBounds = true;
        self.priceLabel.textAlignment = NSTextAlignmentCenter;
        self.canShowCallout = false;
        
        if ([annotation class] == [PriceMapAnnotation class]) {
            PriceMapAnnotation* anno = (PriceMapAnnotation*)annotation;
            self.priceLabel.text = [NSString stringWithFormat:@"%@ %@", anno.price, anno.currencyUnit];
        }
    }
    return self;
}


#pragma mark - Colors
-(UIColor *)borderColor {
    return [UIColor colorWithRed:231.0/255.0 green:91.0/255.0 blue:82.0/255.0 alpha:1];
}

-(UIColor *)unselectedTextColor {
    return [UIColor whiteColor];
}

-(UIColor *)unselectedBackgroundColor {
    return [UIColor colorWithRed:209.0/255.0 green:87.0/255.0 blue:76.0/255.0 alpha:1];
}

-(UIColor *)selectedTextColor {
    return [self borderColor];
}

-(UIColor *)selectedBackgroundColor {
    return [UIColor whiteColor];
}

-(void)setAnnotationSelected:(BOOL)selected {
    PriceMapAnnotation* cast = self.annotation;
    if (selected) {
        self.priceLabel.backgroundColor = [self selectedBackgroundColor];
        self.priceLabel.textColor = [self selectedTextColor];
        
    } else {
        self.priceLabel.backgroundColor = [self unselectedBackgroundColor];
        self.priceLabel.textColor = [self unselectedTextColor];
        
    }
    cast.selected = selected;
}




- (UIImage *)imageFromColor:(UIColor *)color rect:(CGRect)rect {
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

@end
