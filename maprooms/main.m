//
//  main.m
//  maprooms
//
//  Created by hy110831 on 8/2/16.
//  Copyright © 2016 hy110831. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
