//
//  ViewController.m
//  maprooms
//
//  Created by hy110831 on 8/2/16.
//  Copyright © 2016 hy110831. All rights reserved.
//

#import "PriceMapAnnotationView.h"
#import "PriceMapAnnotation.h"
#import "ViewController.h"
#import <Masonry/Masonry.h>
#import <MapKit/MapKit.h>

@interface ViewController()

@property (nonatomic, strong) MKMapView* mapView;
@property (nonatomic, strong) UICollectionView* roomsCollectionView;
@property (nonatomic, weak) PriceMapAnnotationView* currentSelectedView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.mapView = [[MKMapView alloc]init];
    self.mapView.delegate = self;
    [self.view addSubview:self.mapView];
    [self.mapView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    PriceMapAnnotation *annotation = [[PriceMapAnnotation alloc]init];
    annotation.coordinate = CLLocationCoordinate2DMake(34.7725, 113.7266);
    annotation.price = @"100";
    annotation.currencyUnit = @"AUD";
    annotation.selected = false;
    [self.mapView addAnnotation:annotation];

    annotation = [[PriceMapAnnotation alloc]init];
    annotation.coordinate = CLLocationCoordinate2DMake(34.7725, 130.7266);
    annotation.price = @"490";
    annotation.currencyUnit = @"AUD";
    annotation.selected = false;
    [self.mapView addAnnotation:annotation];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma - MapView Delegate

-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
    
    static NSString *reuseId = @"PriceAnnotation";
    PriceMapAnnotationView* pmv = (PriceMapAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:reuseId];
    if (pmv == NULL) {
        pmv = [[PriceMapAnnotationView alloc]initWithAnnotation:annotation reuseIdentifier:reuseId];
    }
    return pmv;
}

-(void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
    if (self.currentSelectedView != nil) {
        [self.currentSelectedView setAnnotationSelected:false];
    }
    [(PriceMapAnnotationView*)view setAnnotationSelected:true];
    self.currentSelectedView = (PriceMapAnnotationView*)view;
}

@end
