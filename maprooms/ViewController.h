//
//  ViewController.h
//  maprooms
//
//  Created by hy110831 on 8/2/16.
//  Copyright © 2016 hy110831. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

@interface ViewController : UIViewController <MKMapViewDelegate>

@property (strong, nonatomic) NSString* destinationName;
@property (assign, nonatomic) CLLocationCoordinate2D targetLatLng;

@end

