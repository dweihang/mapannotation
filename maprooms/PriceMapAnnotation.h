//
//  PriceMapAnnotation.h
//  
//
//  Created by hy110831 on 8/2/16.
//
//

#import <MapKit/MapKit.h>

@interface PriceMapAnnotation : MKPointAnnotation

@property (strong, nonatomic) NSString* price;
@property (strong, nonatomic) NSString* currencyUnit;
@property (assign, nonatomic) BOOL selected;

@end
