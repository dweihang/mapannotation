//
//  AppDelegate.h
//  maprooms
//
//  Created by hy110831 on 8/2/16.
//  Copyright © 2016 hy110831. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

