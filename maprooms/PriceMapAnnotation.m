//
//  PriceMapAnnotation.m
//  
//
//  Created by hy110831 on 8/2/16.
//
//

#import "PriceMapAnnotation.h"

@implementation PriceMapAnnotation


- (instancetype)init
{
    self = [super init];
    if (self) {
        _selected = false;
        _price = @"";
        _currencyUnit = @"";
    }
    return self;
}

@end
