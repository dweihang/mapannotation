//
//  PriceMapAnnotationView.h
//  maprooms
//
//  Created by hy110831 on 8/2/16.
//  Copyright © 2016 hy110831. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface PriceMapAnnotationView : MKAnnotationView

@property (strong, nonatomic) UILabel* priceLabel;

-(void)setAnnotationSelected:(BOOL)selected ;

@end
